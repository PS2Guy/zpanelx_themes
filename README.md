# ZPanelX theme repository.

From here I will store every theme that conforms to the [ZPanelCP] (https://github.com/zpanel/zpanelx) theme standards.

## Theme standards ##

The themes need to have a set structure to be stored here.
The structure is as follows:-

* Theme_Folder
* css
 * default.css
* fonts
* global-css
 * bootstrap.css
 * bootstrap.min.css
 * login.css
* images
* img
* inc
* js
* login.ztml
* master.ztml

***

Download the base theme from [here](https://github.com/PS2Guy/ZPanelX_Themes/blob/master/zpanelx-base/zpanelx-base.zip).
